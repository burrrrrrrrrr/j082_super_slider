import React, {Fragment} from 'react';
import SlideshowState from './context/slideshow/SlideshowState'
import Slideshow from './Components/slideshow/Slideshow'
import NavBar from './Components/layout/NavBar'
import PageContent from './Components/layout/PageContent'
import "circular-std";

function App() {
  return (
    <Fragment>
      <NavBar />
      <SlideshowState>
        <Slideshow />
      </SlideshowState>
      <PageContent />
    </Fragment>
  );
}

export default App;
