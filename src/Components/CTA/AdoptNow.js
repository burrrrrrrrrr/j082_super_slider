import React, { useContext } from 'react'
import SlideshowContext from '../../context/slideshow/slideshowContext'

export const AdoptNow = () => {
    const slideshowContext = useContext(SlideshowContext)
    const { setAdoptNowOpen, adoptNowOpen } = slideshowContext

    const onClickAdopt = e => {
        console.log('onclick')
        console.log(setAdoptNowOpen)
        e.stopPropagation();
        if(adoptNowOpen) {
            setAdoptNowOpen(false)
        } else {
            setAdoptNowOpen(true)
        }
    }

    return (
        <div className={ "adopt-now " + (!adoptNowOpen ? 'apopt-now-closed' : 'apopt-now-open') }>
            <div className="adopt-now-pay-btns">
                <span className="adopt-now-monthly-amount">MONTHLY AMOUNT</span>
                <button className="adopt-now-pay-btn">£3</button>
                <button className="adopt-now-pay-btn">£5</button>
                <button className="adopt-now-pay-btn">£10</button>
            </div>
            <button onClick={onClickAdopt} className="adopt-now-btn">ADOPT NOW</button>
        </div>
    )
}

export default AdoptNow
