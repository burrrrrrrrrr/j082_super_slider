import React, { useContext} from 'react'
import SlideshowContext from '../../context/slideshow/slideshowContext'
import Slide from './Slide'
import SlideDots from './SlideDots'
import image0 from '../../assets/lion.jpg'
import image1 from '../../assets/jaguar.png'
import image2 from '../../assets/tiger.jpg'
import textTrackTitle0 from '../../assets/LION.svg'
import textTrackTitle1 from '../../assets/JAGUAR.svg'
import textTrackTitle2 from '../../assets/TIGER.svg'

const globalSlides = [
    {
        "id": 0,
        "image": image0,
        "slideTitle": "Adopt A",
        "textTrackTitle": textTrackTitle0,
        "textTrackTitleWidth": 298,
        "contentTitle1": "HABITAT LOSS",
        "contentText1": "Wild tigers are hunted to meet the demands of the $20 billion a year illegal wildlife market.",
        "contentTitle2": "LOSS OF PREY",
        "contentText2": "Deer and wild pigs, continue to be overhunted, forcing tigers to attack livestock to feed themselves and their cubs, thus fueling human-tiger conflict.",
        "contentTitle3": "POACHING",
        "contentText3": "Wild tigers are persecuted when villagers take retaliatory measures to protect their livestock and communities.",
    },
    {
        "id": 1,
        "image": image1,
        "textTrackTitle": textTrackTitle1,
        "textTrackTitleWidth": 520,
        "contentTitle1": "HABITAT LOSS",
        "contentText1": "In the Amazon an area of rainforest the size of three football pitches is lost every minute.",
        "contentTitle2": "LOSS OF PREY",
        "contentText2": "People hunt the jaguars’ natural prey – so they see jaguars as competition for food.",
        "contentTitle3": "POACHING",
        "contentText3": "Still poached for their paws, teeth and other parts, which are generally used in traditional Asian medicines.",
    },
    {
        "id": 2,
        "image": image2,
        "textTrackTitle": textTrackTitle2,
        "textTrackTitleWidth": 161,
        "contentTitle1": "DISEASE",
        "contentText1": "Lion populations have been irreparably affected by disease in the last few years.",
        "contentTitle2": "CLIMATE CHANGE",
        "contentText2": "many of the lion populations across Africa are confined to game reserves and national parks",
        "contentTitle3": "WILDLIFE TRADE",
        "contentText3": "The illegal trade of wildlife is the fourth biggest illegal trade in the world.", 
    }
]

export const Slideshow = () => {
    const slideshowContext = useContext(SlideshowContext)
    const { initialPosition, dragPosition, setInitialPosition, setDragPosition, setCurrent} = slideshowContext

    const getSlideIndex = (increment) => {
        let nextPosition = slideshowContext.slideIndex += increment
        if(nextPosition < 0) {
            nextPosition = globalSlides.length - 1
        }
        if(nextPosition > globalSlides.length - 1) {
            nextPosition = 0
        }
        return nextPosition
    }

    const moveSlide = increment => {
        setCurrent(getSlideIndex(increment))
    }

    const mouseDown = e => {
        setInitialPosition(e.pageX)
        setDragPosition(e.pageX)
    }

    const mouseMove = e => {
        if(initialPosition !== null) {
            setDragPosition(e.pageX)
        }
    }

    const mouseUp = e => {
        if(dragPosition - initialPosition > window.innerWidth / 2 && Math.abs(dragPosition - initialPosition) > window.innerWidth / 2) {
            moveSlide(+1)
        }
        if(dragPosition - initialPosition < window.innerWidth / 2 && Math.abs(dragPosition - initialPosition) > window.innerWidth / 2) {
            moveSlide(-1)
        }
        setDragPosition(0)
        setInitialPosition(null)
    }

    return (
        <div className="slideshow" onMouseDown={ mouseDown } onMouseMove={ mouseMove } onMouseUp={ mouseUp } >
            <Slide />
            <SlideDots />
        </div>
    )
}

export default Slideshow