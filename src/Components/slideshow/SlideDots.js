/* eslint-disable jsx-a11y/anchor-has-content */
import React, {useContext} from 'react'
import SlideshowContext from '../../context/slideshow/slideshowContext'

export const SlideDots = () => {
    const slideshowContext = useContext(SlideshowContext)

    const { slides, slideIndex, setCurrent } = slideshowContext

    const setSlide = (e) => {
        e.preventDefault();
        setCurrent(e.target.dataset.slideDot)
    }

    return (
        <div className="slide-dots">
            <ul>
                {slides.map(function(slide, i){
                    return (   
                        <li>
                            <a key={i} data-slide-dot={i} onClick={ setSlide } className={"slide-dot " + (parseInt(slideIndex) === i ? 'active' : '')} href="#/" aria-label="nav-dot"></a>
                        </li>)
                })}
            </ul>
        </div>
    )
}

export default SlideDots