import React, {useContext} from 'react'
import SlideshowContext from '../../context/slideshow/slideshowContext'
import TextTrack from './TextTrack'
import SlideText from './SlideText'
import adopta from '../../assets/ADOPTA.png'
import AdoptNow from '../../Components/CTA/AdoptNow'

export const Slide = (props) => {
    const slideshowContext = useContext(SlideshowContext)

    const { slides, slideIndex } = slideshowContext

    const data = slides[slideIndex]

    const slideStyles = {
        backgroundImage: `url(${data.image})`
    }

    return (
        <div className="slide" style={slideStyles}>
            <div className="slide-container">
                <div className="slide-inner">
                    <div className="top-row slide-row">
                        <img className="adopta-img" src={adopta} alt="adopt a" />
                        <SlideText classIndex={"1"} titleText={data.contentTitle1} textContent={data.contentText1} />
                    </div>
                    <TextTrack data={data} slides={data} />
                    <div className="bottom-row slide-row">
                        <SlideText classIndex={"2"} titleText={data.contentTitle2} textContent={data.contentText2} />
                        <SlideText classIndex={"3"} titleText={data.contentTitle3} textContent={data.contentText3} />
                    </div>
                </div>
                <AdoptNow />
            </div>
        </div>
    )
}

export default Slide