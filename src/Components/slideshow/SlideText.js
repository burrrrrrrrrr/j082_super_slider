import React, {useContext} from 'react'
import SlideshowContext from '../../context/slideshow/slideshowContext'

export const SlideText = ({titleText, textContent, classIndex}) => {
    const slideshowContext = useContext(SlideshowContext)
    const { initialPosition, dragPosition} = slideshowContext

    const dragPercentage = (dragPosition - initialPosition) / window.innerWidth * 200

    var overlayStyles = {left: 0, width: dragPercentage+"%"}

    if(dragPercentage > 0) {
        var overlayStyles = {left: 0, width: dragPercentage+"%"}

    } else {
        var overlayStyles = {right: 0, width: Math.abs(dragPercentage)+"%"}
    }

    if (dragPercentage === 0) {
        var overlayStyles = {left: 0, right: "auto"}
    }

    return (
        <section className="text-wrapper">
            <div className={"text text-"+classIndex}>
                <h4>{titleText}
                    <span className="text-overlay" style={overlayStyles}></span>
                </h4>
                <p>{textContent}
                    <span className="text-overlay" style={overlayStyles}></span>
                </p>
            </div>
        </section>
    )
}

export default SlideText