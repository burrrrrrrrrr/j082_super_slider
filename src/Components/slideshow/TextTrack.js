import React, { useContext} from 'react'
import { ReactSVG } from 'react-svg'
import SlideshowContext from '../../context/slideshow/slideshowContext'

export const TextTrack = (props) => {
    const slideshowContext = useContext(SlideshowContext)

    const { slides, dragPosition, initialPosition, slideIndex,  } = slideshowContext

    const containerWidth = 1000;

    const windowWidth = window.innerWidth;

    const gutterWidth = (windowWidth - containerWidth) / 2

    const elemLeftPosition = [0,0,0]

    const slideStyles = {
        left: `${(dragPosition - initialPosition)}px`,
        width: `${window.innerWidth}px`,
    }

    switch (parseInt(slideIndex)) {
        case 0:
            var trackTitles = [slides[2].textTrackTitle, slides[0].textTrackTitle, slides[1].textTrackTitle]
            var leftOffset = slides[2].textTrackTitleWidth
            break;
        case 1:
            var trackTitles = [slides[0].textTrackTitle, slides[1].textTrackTitle, slides[2].textTrackTitle]
            var leftOffset = slides[0].textTrackTitleWidth
            break;
        case 2:
            var trackTitles = [slides[1].textTrackTitle, slides[2].textTrackTitle, slides[0].textTrackTitle]
            var leftOffset = slides[1].textTrackTitleWidth
            break;
        default:
            break;
    }
    

    elemLeftPosition[0] = -gutterWidth + parseInt(-leftOffset) + 150 + "px"

    elemLeftPosition[1] = 0 + "px"

    elemLeftPosition[2] = `calc(100vw - ${150 + gutterWidth}px)`

    if(initialPosition === null) {
        slideStyles['transition'] = '.2s left ease-in-out'
    }

    return (
        <div className="text-track">
            <ul className="text-track-inner" style={slideStyles}>
                {trackTitles.map(function(title, idx){
                    return (   
                        <li key={idx} data-key={idx} id={`text-track-item-${idx}`} style={{left: elemLeftPosition[idx]}}>
                            <ReactSVG src={title} />
                        </li>)
                })}
            </ul>
        </div>
    )
}

export default TextTrack