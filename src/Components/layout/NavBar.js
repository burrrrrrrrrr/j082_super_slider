import React from 'react'
import logo from './../../assets/logo.svg'
import { ReactSVG } from 'react-svg'

export const NavBar = () => {
    return (
        <nav className="navbar">
            <ul className="">
                <li><a href="#/"><ReactSVG src={logo} /></a></li>
                <li><a href="#/">Adopt</a></li>
                <li><a href="#/">Donate</a></li>
                <li><a href="#/">Join</a></li>        
            </ul>    
            <ul>
                <li><a href="#/">Search</a></li>
                <li><a href="#/">Account</a></li>
                <li><a href="#/">Cart</a></li>
                <li><a href="#/" className="cart-counter">1</a></li>
            </ul>    
        </nav>
    )
}

export default NavBar