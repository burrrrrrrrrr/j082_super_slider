import React from 'react'

export const PageContent = () => {
    return (
        <div className="slide-container page-content">
            <h2>Page Content :)</h2>
        </div>
    )
}

export default PageContent
