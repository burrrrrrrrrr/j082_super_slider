import {
    SET_CURRENT,
    GET_SLIDES_DATA,
    SET_DRAG_POSITION,
    SET_INITIAL_POSITION,
    SET_ADOPT_NOW_OPEN
 } from '../../types'

export default (state, action) => {
    switch (action.type) {
        case SET_CURRENT:
            return {
                ...state, 
                slideIndex: action.payload
            }
        case GET_SLIDES_DATA:
            return {
                ...state, 
                slideIndex: action.payload
            }
        case SET_DRAG_POSITION:
            return {
                ...state,
                dragPosition: action.payload 
            }
        case SET_INITIAL_POSITION:
            return {
                ...state,
                initialPosition: action.payload
            }
        case SET_ADOPT_NOW_OPEN:
            return {
                ...state,
                adoptNowOpen: action.payload                
            }
        default:
            return state
    }
}
