import React, { useReducer } from 'react'
import SlideshowContext from './slideshowContext'
import slideshowReducer from './slideshowReducer'
import image0 from '../../assets/lion.jpg'
import image1 from '../../assets/jaguar.png'
import image2 from '../../assets/tiger.jpg'
import textTrackTitle0 from '../../assets/LION.svg'
import textTrackTitle1 from '../../assets/JAGUAR.svg'
import textTrackTitle2 from '../../assets/TIGER.svg'

import { 
SET_CURRENT,
SET_DRAG_POSITION,
SET_INITIAL_POSITION,
SET_SLIDE_IS_GRABBED,
SET_ADOPT_NOW_OPEN,
} from '../../types'

const SlideshowState = props => {
    const initialState = {
        slideIndex: 1,
        initialPosition: null,
        dragPosition: 0,
        adoptNowOpen: false,
        slides: [
    {
        "id": 0,
        "image": image0,
        "slideTitle": "Adopt A",
        "textTrackTitle": textTrackTitle0,
        "textTrackTitleWidth": 298,
        "contentTitle1": "DISEASE",
        "contentText1": "Lion populations have been irreparably affected by disease in the last few years.",
        "contentTitle2": "CLIMATE CHANGE",
        "contentText2": "many of the lion populations across Africa are confined to game reserves and national parks",
        "contentTitle3": "WILDLIFE TRADE",
        "contentText3": "The illegal trade of wildlife is the fourth biggest illegal trade in the world.", 
    },
    {
        "id": 1,
        "image": image1,
        "textTrackTitle": textTrackTitle1,
        "textTrackTitleWidth": 520,
        "contentTitle1": "HABITAT LOSS",
        "contentText1": "In the Amazon an area of rainforest the size of three football pitches is lost every minute.",
        "contentTitle2": "LOSS OF PREY",
        "contentText2": "People hunt the jaguars’ natural prey – so they see jaguars as competition for food.",
        "contentTitle3": "POACHING",
        "contentText3": "Still poached for their paws, teeth and other parts, which are generally used in traditional Asian medicines.",
    },
    {
        "id": 2,
        "image": image2,
        "textTrackTitle": textTrackTitle2,
        "textTrackTitleWidth": 161,
        "contentTitle1": "HABITAT LOSS",
        "contentText1": "Wild tigers are hunted to meet the demands of the $20 billion a year illegal wildlife market.",
        "contentTitle2": "LOSS OF PREY",
        "contentText2": "Deer and wild pigs, continue to be overhunted, forcing tigers to attack livestock to feed themselves and their cubs, thus fueling human-tiger conflict.",
        "contentTitle3": "POACHING",
        "contentText3": "Wild tigers are persecuted when villagers take retaliatory measures to protect their livestock and communities.",
    }
        ]
    }
    
    const [state, dispatch] = useReducer(slideshowReducer, initialState)

    // Set Current Slide
    const setCurrent = slide => {
        dispatch({type: SET_CURRENT, payload: slide})
    }

    // Set Drag Position 
    const setDragPosition = textTrack => {
        dispatch({type: SET_DRAG_POSITION, payload: textTrack})
    }

    // Set Initial Mouse Down / Drag Position 
    const setInitialPosition = textTrack => {
        dispatch({type: SET_INITIAL_POSITION, payload: textTrack})
    }

    // Set Dragged to Position 
    const setSlideDragPosition = textTrack => {
        dispatch({type: SET_SLIDE_IS_GRABBED, payload: textTrack})
    }

    // Set Adopt nw button open 
    const setAdoptNowOpen = adoptNowOpen => {
        dispatch({type: SET_ADOPT_NOW_OPEN, payload: adoptNowOpen})
    }

    return (
        <SlideshowContext.Provider
            value={{
                slideIndex: state.slideIndex,
                slides: state.slides,
                dragPosition: state.dragPosition,
                initialPosition: state.initialPosition,
                slideDragPosition: state.slideDragPosition,
                adoptNowOpen: state.adoptNowOpen,
                setCurrent,
                setInitialPosition,
                setSlideDragPosition,
                setAdoptNowOpen,
                setDragPosition
            }}
        >
            {props.children}
        </SlideshowContext.Provider>
    )
}

export default SlideshowState