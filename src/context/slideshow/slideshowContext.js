import { createContext } from 'react'

const slideshowContext = createContext()

export default slideshowContext